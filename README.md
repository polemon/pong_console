# pong_console

Recreation of the good old Pong Console, based around the AY-3-8500 game IC.

***

![Pong Console "Pongu"](./pong_console_3D.png)*Pong Console "Pongu"*

## Description
Based around the famous AY-3-8500 game IC, this is primarily a PCB recreation of countless versions of Pong consoles.

Controllers are two 1kΩ potentionmeters, attached to the ports via 2.5mm TS jacks.

## License
OSH
